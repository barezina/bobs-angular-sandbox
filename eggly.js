angular.module('Eggly', [

])
.controller('MainController', function($scope) {

    $scope.title = "Hello world";

    $scope.categories = [
        {"id": 0, "name": "Development"},
        {"id": 1, "name": "Testing"},
        {"id": 2, "name": "Production"}
    ];

    $scope.bookmarks = [
        {"id": 0, "name": "PHP", "category": "Development"},
        {"id": 1, "name": "MySQL", "category": "Development"},
        {"id": 2, "name": "PHPUnit", "category": "Testing"},
        {"id": 3, "name": "Selenium", "category": "Testing"},
        {"id": 4, "name": "Jenkins", "category": "Production"},
        {"id": 5, "name": "AWS", "category": "Production"}
    ];

    $scope.currentCategory = null;
    $scope.currentBookmarkState = null;
    $scope.newBookmark = null;

    function setCurrentCategory(category) {
        $scope.currentCategory = category;
    }

    function setCurrentBookmarkState(bookmarkState, incomingBookmark) {
        $scope.currentBookmarkState = bookmarkState;
        $scope.bookmarkToStore = angular.copy(incomingBookmark);
    }

    function shouldShowEditor() {
        if($scope.currentBookmarkState === 'Create' || $scope.currentBookmarkState === 'Edit') {
            return true;
        } else {
            return false;
        }
    }

    function storeBookmark(bookmark) {

        if($scope.currentBookmarkState === 'Create') {
            bookmark.id = $scope.bookmarks.length;
            $scope.bookmarks.push(angular.copy(bookmark));
        } else {
            var index = _.findIndex($scope.bookmarks, function(b) {
                return b.id == bookmark.id;
            });
            $scope.bookmarks[index] = bookmark;
        }
        $scope.currentBookmarkState = null;
    }

    function deleteBookmark(bookmark) {
        _.remove($scope.bookmarks, function(b) {
            return b.id == bookmark.id;
        });
    }

    $scope.setCurrentCategory = setCurrentCategory;
    $scope.setCurrentBookmarkState = setCurrentBookmarkState;
    $scope.shouldShowEditor = shouldShowEditor;
    $scope.storeBookmark = storeBookmark;
    $scope.deleteBookmark = deleteBookmark;

});